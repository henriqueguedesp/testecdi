const express = require('express');
//importação do morgan para visualização de log de requisição da api
const morgan = require('morgan');
//biblioteca para acesso remoto
const cors = require('cors');

//iniciando o app
const app = express();

//habilitando o recebimento de arquivos json
app.use(express.json());
//habilitando o morgan para monitoramento da api
app.use(morgan('combined'));

//liberação dos metodos HTTP
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Methods", "GET,PUT,POST,PATCH");
	res.header("Access-Control-Allow-Headers", "Content-Type");
	next();
});

//importação de rotas
app.use('/', require('./src/routes/index'));
//importação de rotas de produtos
require("./src/routes/produto/produto")(app);

app.listen(3000);