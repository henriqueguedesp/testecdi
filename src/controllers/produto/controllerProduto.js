//realizando importação dos modelos
const model = require('../../models/index');
//importando yup (validação de formulários)
const yup = require("yup");

//função para cadastar produto
exports.cadastrar = (req, res) => {
    //definição do esquema 
    const schema = yup.object().shape({
        //nas duas linhas abaixo segue código onde desejamos que seja validados os campos
        nome: yup.string().required().min(5),
        descricao: yup.string().required().min(5)
    });
    try {
        schema.validate(req.body)
            .then(async valid => {
                const { nome, descricao } = req.body;
                await model.produtos.create({
                    nome: nome,
                    descricao: descricao
                }).then(result => {
                    return res.status(200).send(result);
                }).catch(error => {
                    return res.status(400).send({ error: true, message: error });
                });
            }).catch(error => {
                return res.status(400).send({ error: true, message: error });
            });
    } catch (error) {
        return res.status(400).send({ error: true, message: erro });
    }
}

//função para listar todos os produtos cadastros
exports.listar = async (req, res) => {
    try {
        await model.produtos.findAll({})
            .then(result => {
                console.log(result);
                return res.status(200).send(result)
            }).catch(error => {
                return res.status(400).send({ error: true, message: error });
            });
    }
    catch (error) {
        return res.status(400).send({ error: true, message: error });
    }
}

//função para atualizar um registro
exports.atualizar = (req, res) => {
    const id = req.params.id;
    console.log(req.body);
    const schema = yup.object().shape({
        nome: yup.string().required().min(5),
        descricao: yup.string().required().min(5)
    });
    const { nome, descricao } = req.body;
    try {
        schema.validate({ id, nome, descricao })
            .then(async valid => {
                await model.produtos.update(
                    {
                        nome: nome,
                        descricao: descricao
                    },
                    { where: { id: id } }
                ).then(result => {
                    return res.status(200).send(result);
                }).catch(error => {
                    return res.status(400).send({ error: true, message: error });
                });
            }).catch(error => {
                return res.status(400).send({ error: true, message: error });
            });
    } catch (error) {
        return res.status(400).send({ error: true, message: error });
    }
}
