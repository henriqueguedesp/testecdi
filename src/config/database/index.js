const { Client } = require("pg");

require("dotenv").config();
 /*
console.log(process.env.DB_DATABASE);
console.log(process.env.DB_USER);
console.log(process.env.DB_PASS);
console.log(process.env.NODE_ENV);
*/

const client = new Client({
    user: process.env.NODE_ENV === "development" ? process.env.DB_USER_HML : process.env.DB_USER,
    host: process.env.NODE_ENV === "development" ?  process.env.DB_HOST_HML : process.env.DB_HOST,
    database: process.env.NODE_ENV === "development" ?  process.env.DB_DATABASE_HML : process.env.DB_DATABASE,
    password: process.env.NODE_ENV === "development" ?  process.env.DB_PASS_HML : process.env.DB_PASS,
    port: process.env.NODE_ENV === "development" ?  process.env.DB_PORT_HML : process.env.DB_PORT ,
});

client.connect();

module.exports = {
    query: (text, params) => client.query(text, params),
};