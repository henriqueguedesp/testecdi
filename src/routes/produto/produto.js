module.exports = app =>{
    const express = require("express");
    const produto = require("../../controllers/produto/controllerProduto");
    const router = express.Router();
    
    router.post("/",produto.cadastrar);
    router.get("/",produto.listar);
    router.put("/:id",produto.atualizar);

    app.use("/produto",router);
}